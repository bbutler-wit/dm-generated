# dm-generated overview

This project contains example XACML 2.0 policies and requests generated by the `DomainManager` application, based on policy specifications in the `config` directory.
